package sbu.cs;

public class ExerciseLecture4 {

    /*
     *   implement a function that returns factorial of given n
     *   lecture 4 page 15
     */
    public long factorial(int n) {
        long factorial = 1;
        for(int i = 1 ; i <= n ; i++) {
            factorial = factorial * i;
        }
        return factorial;
    }

    /*
     *   implement a function that return nth number of fibonacci series
     *   the series -> 1, 1, 2, 3, 5, 8, ...
     *   lecture 4 page 19
     */
    public long fibonacci(int n) {
        if(n==0) {
            return 0;
        }
        else if(n==1) {
            return 1;
        }
        else {
            return(fibonacci(n-2) + fibonacci(n-1));
        }
    }

    /*
     *   implement a function that return reverse of a given word
     *   lecture 4 page 19
     */
    public String reverse(String word) {
        String reverse = "";
        for(int i = word.length() - 1 ; i >= 0 ; i--) {
            reverse = reverse + word.charAt(i);
        }
        return reverse;
    }

    /*
     *   implement a function that returns true if the given line is
     *   palindrome and false if it is not palindrome.
     *   palindrome is like 'wow', 'never odd or even', 'Wow'
     *   lecture 4 page 19
     */
    public boolean isPalindrome(String line) {
        String withoutspace = "";
        for(int i = 0 ; i < line.length() ; i++) {
            if(line.charAt(i) != ' ') {
                withoutspace = withoutspace + line.charAt(i);
            }
        }
        withoutspace = withoutspace.toLowerCase();
        if(withoutspace.equals(reverse(withoutspace)))
        {
            return true;
        }
            return false;
    }

    /*
     *   implement a function which computes the dot plot of 2 given
     *   string. dot plot of hello and ali is:
     *       h e l l o
     *   h   *
     *   e     *
     *   l       * *
     *   l       * *
     *   o           *
     *   lecture 4 page 26
     */
    public char[][] dotPlot(String str1, String str2) {
        char[][] stars = new char[str1.length()][str2.length()];
        for(int i = 0 ; i < str1.length() ; i++) {
            for(int j = 0 ; j < str2.length() ; j++) {
                if(str1.charAt(i) == str2.charAt(j)) {
                    stars[i][j] = '*';
                }
                else {
                    stars[i][j] = ' ';
                }
            }
        }
        return stars;
    }
}
