package sbu.cs;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
public class ExerciseLecture6 {

    /*
     *   implement a function that takes an array of int and return sum of
     *   elements at even positions
     *   lecture 6 page  16
     */
    public long calculateEvenSum(int[] arr) {
        long sum = 0;
        for(int i = 0 ; i < arr.length ; i = i+2) {
            sum = sum + arr[i];
        }
        return sum;
    }

    /*
     *   implement a function that takes an array of int and return that
     *   array in reverse order
     *   lecture 6 page 16
     */
    public int[] reverseArray(int[] arr) {
        int [] reverse = new int[arr.length];
        int j = 0;
        for(int i = arr.length-1 ; i >=0 ; i--) {
            reverse[j] = arr[i];
            j++;
        }
        return reverse;
    }

    /*
     *   implement a function that calculate product of two 2-dim matrices
     *   lecture 6 page 21
     */
    public double[][] matrixProduct(double[][] m1, double[][] m2) throws RuntimeException {
        if((m1[0].length != m2.length)||( m1[0].length != m2.length)){
            throw new RuntimeException();
        }
        double temp = 0;
        double[][] product = new double[m1.length][m2[0].length];
        for(int i = 0 ; i < m1.length ;i++) {
            for (int k = 0; k < m2[0].length; k++) {
                for (int j = 0; j < m2.length; j++) {
                    temp = temp + (m1[i][j] * m2[j][k]);
                }
                product[i][k] = temp;
                temp = 0;
            }
        }
        return product;
    }

    /*
     *   implement a function that return array list of array list of string
     *   from a 2-dim string array
     *   lecture 6 page 30
     */
    public List<List<String>> arrayToList(String[][] names) {
        List<List<String>> listOfLists = new ArrayList<List<String>>();
        for(int i = 0 ; i < names.length ; i++) {
            List<String> temp = new ArrayList<String>();
            for(int j = 0 ; j < names[0].length ; j++) {
                temp.add(names[i][j]);
            }
            listOfLists.add(temp);
        }
        return listOfLists;
    }

    /*
     *   implement a function that return a list of prime factor of integer n
     *   in ascending order
     *   lecture 6 page 30
     */
    public List<Integer> primeFactors(int n) {
        ArrayList<Integer> primefactors = new ArrayList<Integer>();
        int i = 2;
        while(n > 1) {
            while(n % i == 0) {
                if(!primefactors.contains(i)) {
                    primefactors.add(i);
                }
                n = n / i;
            }
            i++;
        }
        Collections.sort(primefactors);
        return primefactors;
    }

    /*
     *   implement a function that return a list of words in a given string
     *   lecture 6 page 30
     */
    public List<String> extractWord(String line) {
        line = line.replaceAll("[\\.\\?\\!\\,]","");
        String[] str = line.split(" ");
        List<String> list = Arrays.asList(str);
        return list;
    }
}
