package sbu.cs;

public class TallyCounter implements TallyCounterInterface {
    private int counter;

    @Override
    public void count() {
        counter++;
        if(counter > 9999) {
            counter = 9999;
        }
    }

    @Override
    public int getValue() {
        return counter;
    }

    @Override
    public void setValue(int value) throws IllegalValueException {
        if(value > 9999 || value < 0) {
            throw new IllegalValueException();
        }
        counter = value;
    }
}
