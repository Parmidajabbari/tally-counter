package sbu.cs;
import java.util.Random;
public class ExerciseLecture5 {

    /*
     *   implement a function to create a random password with
     *   given length using lower case letters
     *   lecture 5 page 14
     */
    public String weakPassword(int length) {
        String pass = "";
        Random r = new Random();
        for (int i = 0; i < length; i++) {
            char c = (char) (r.nextInt(26) + 'a');
            pass = pass + c;
        }
        return pass;
    }

    /*
     *   implement a function to create a random password with
     *   given length and at least 1 digit and 1 special character
     *   lecture 5 page 14
     */
    public String strongPassword(int length) throws Exception {
        if (length < 3) {
            throw new IllegalValueException();
        }
        Random r = new Random();
        StringBuilder pass = new StringBuilder();
        String alphabet = "abcdefghijklmnopqrstuvwxyz";
        String numbers = "1234567890";
        String characters = "!@#$%^&*()+";
        int rand_int = r.nextInt(alphabet.length());
        pass.append(alphabet.charAt(rand_int));
        rand_int = r.nextInt(numbers.length());
        pass.append(numbers.charAt(rand_int));
        rand_int = r.nextInt(characters.length());
        pass.append(characters.charAt(rand_int));
        String random = alphabet + numbers + characters;
        int n = random.length();
        for (int i = 0; i < length - 3; i++) {
            int rand_int1 = r.nextInt(n);
            pass.append(random.charAt(rand_int1));
        }
        return pass.toString();
    }

    /*
     *   implement a function that checks if a integer is a fibobin number
     *   integer n is fibobin is there exist an i where:
     *       n = fib(i) + bin(fib(i))
     *   where fib(i) is the ith fibonacci number and bin(i) is the number
     *   of ones in binary format
     *   lecture 5 page 17
     */
    public int Bin(int n) {
        String number = "";
        Integer n1 = new Integer(n);
        while (n1 > 1) {
            if (n1 % 2 == 0) {
                number = '0' + number;
            } else {
                number = '1' + number;
            }
            n1 = n1 / 2;
        }
        number = n1.toString() + number;
        int foo = Integer.parseInt(number);
        return foo;
    }

    public long fibonacci(int n) {
        if (n == 0) {
            return 0;
        } else if (n == 1) {
            return 1;
        } else {
            return (fibonacci(n - 2) + fibonacci(n - 1));
        }
    }

    public boolean isFiboBin(int n) {
        for (int i = 0; i < n; i++) {
            if (fibonacci(i) + Bin(i) == n) {
                return true;
            }
        }
        return false;
    }
}
